using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using EventApp.Models;
using EventApp.Models.Mappings;
using EventApp.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EventApp.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class GroupController : ControllerBase
    {
        private readonly IUserGroupService _userGroupService;
        private readonly IPersonService _personService;
        private readonly IGroupService _groupService;

        public GroupController(IUserGroupService userGroupService, IPersonService personService, IGroupService groupService)
        {
            _groupService = groupService;
            _userGroupService = userGroupService;
            _personService = personService;
        }
        
        
        [HttpGet("{groupId}")]
        [ProducesResponseType(typeof(IEnumerable<Group>), 200)]
        [ProducesResponseType(500)]
        public ActionResult<IEnumerable<UserGroup>> get(int groupId)
        {
            return Ok(_groupService.GetGroupAsync(groupId));
        }

        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(500)]
        /*
         * 3. feladat
         *     [POST] http://localhost:5000/api/group/create
         * request body-ba kell belerakni
         * => 
         * {
                "GroupName": "Group",
                "GroupType": "1",
                "Town" : "Veszprem"
            }
         */
        
        public async Task<IActionResult> Create([FromBody] Group group)
        {
            group.Administrator = await _personService.GetPersonByName(User.Identity.Name);
            await _groupService.CreateGroupAsync(group);
            return Ok("Created!");
        }
        
              
        /**
         * 3. feladat group törlése csak a group administrator teheti meg
         * request:
         * [DELETE] http://localhost:5000/api/group/delete/8 
         */
        [HttpDelete("{groupId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Delete(int groupId)
        {
            var group = await _groupService.GetGroupAsync(groupId);
            if (group != null && group.Administrator.Id == _personService.GetPersonByName(User.Identity.Name).Id)
            {
                await _personService.DeletePersonGroups(groupId);
                await _groupService.DeleteGroupAsync(groupId);
                return Ok("Group deleted!");
            }

            return BadRequest("You're not the administrator of this group, so you can't delete it!");
        }
        
    }

}