using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using EventApp.Models;
using EventApp.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EventApp.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UserGroupController : ControllerBase
    {
        private readonly IUserGroupService _userGroupService;
        private readonly IPersonService _personService;
        private readonly IGroupService _groupService;
        private readonly IGroupInvitationService _groupInvitationService;

        public UserGroupController(IUserGroupService userGroupService, IPersonService personService, IGroupService groupService, IGroupInvitationService groupInvitationService)
        {
            _userGroupService = userGroupService;
            _personService = personService;
            _groupService = groupService;
            _groupInvitationService = groupInvitationService;
        }
        /* 3. feladat user hozzáadása egy grouphoz
         *[POST] http://localhost:5000/api/usergroup/AddUserToGroup/5
         * request body-ba:
         * "admin" megadjuk a user nevét akit hozzáakarunk adni a grouphoz
         * 
         */
        [HttpPost("{groupId}")]
        [ProducesResponseType(201)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> AddUserToGroup([FromBody] string username, int groupId)
        {
            var group = _groupService.GetGroupAsync(groupId).Result;
            var user = _personService.GetPersonByName(username).Result;
            if (group.GroupType == 1) //nyitott csoport akárki tud csatlakozni
            {
                var userGroup = new UserGroup();
                userGroup.Group = group;
                userGroup.Person = user;
                group.UserGroups.Add(userGroup);
                user.UserGroups.Add(userGroup);
                await _groupService.UpdateGroupAsync(group);
                await _personService.UpdatePersonAsync(user);
                return Ok("Done");
            }
            //closed group
            var foundGroup = _groupService.GetGroupAsync(groupId).Result;
            //zárt csoport új rekord kerül a group invitation táblába, amit  a user eltud fogadni
            if ((foundGroup.UserGroups
                .Exists(userGroup => user.ApplicationUser.UserName == username) || foundGroup.Administrator.ApplicationUser.UserName == User.Identity.Name)
                && !foundGroup.UserGroups.Exists(userGroup => userGroup.UserId == user.Id)
                )
            {
                var groupInvitation = new GroupInvitation {PersonId = user.Id, GroupId = group.Id, Status = InvitationStatus.Created};
                await _groupInvitationService.CreateGroupInvitationAsync(groupInvitation);
                return Ok("");
            }

            return BadRequest("Something went wrong!");
        }
        
        /**
         *  3. feladat user elfogadja/elutasítja a meghívást
         * Ha a usernek van függőben lévő elfogadandó group invitationja akkor eltudja fogadni
         * [POST] http://localhost:5000/api/usergroup/AcceptGroupInvitation/5
         * request body-ba=>
         * vagy "accept" ha elakarja fogadni vagy "cancell"
         */
        [HttpPost("{groupId}")]
        [ProducesResponseType(201)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> AcceptGroupInvitation([FromBody] string acceptance, int groupId)
        {
            var group = _groupService.GetGroupAsync(groupId).Result;
            var groupInvitation = await _groupInvitationService.GetByGroupIdAndUserId(groupId, _personService.GetPersonByName(User.Identity.Name).Result.Id);
            if (acceptance == "accept")
            {
                if (groupInvitation != null && groupInvitation.Status == InvitationStatus.Created)
                {
                    var userGroup = new UserGroup();
                    userGroup.Person = groupInvitation.Person;
                    userGroup.Group = groupInvitation.Group;
                    groupInvitation.Status = InvitationStatus.Accepted;
                    await _groupInvitationService.UpdateGroupInvitationAsync(groupInvitation);
                    group.UserGroups.Add(userGroup);
                    groupInvitation.Person.UserGroups.Add(userGroup);
                    await _personService.UpdatePersonAsync(groupInvitation.Person);
                    await _groupService.UpdateGroupAsync(group);
                    return Ok("Accepted!");
                }  
            }
            else
            {
                groupInvitation.Status = InvitationStatus.Declined;
                await _groupInvitationService.UpdateGroupInvitationAsync(groupInvitation);
                return Ok("Declined!");
            }

            return BadRequest("Something went wrong!");
        }
        
        /**
         3. feladat User törlése a groupból ezt csak a group administrátora tudja végrehajtani
           [DELETE] http://localhost:5000/api/usergroup/DeleteUserFromGroup/6
           request body => 
           "admin"
         */
        [HttpDelete("{groupId}")]
        [ProducesResponseType(201)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> DeleteUserFromGroup([FromBody] string username, int groupId)
        {
            var group = _groupService.GetGroupAsync(groupId).Result;
            if (User.Identity.Name != @group.Administrator.ApplicationUser.UserName || username == User.Identity.Name)
                return BadRequest("You're not the administrator of this group so, you cannot remove user from it!");
            var userGroup = new UserGroup();
            var user = _personService.GetPersonByName(username).Result;
            userGroup.Person = user;
            userGroup.Group = @group;
            var foundUserGroup = user.UserGroups.Find(ug => ug.GroupId == groupId);
            if (foundUserGroup != null)
            {
                user.UserGroups.Remove(foundUserGroup);
                await _personService.UpdatePersonAsync(user);
                return Ok("Deleted successfully!"); 
            }

            return BadRequest("Something is wrong!");
        }
    }
}