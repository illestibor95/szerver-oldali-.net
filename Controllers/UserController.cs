using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EventApp.Models;
using EventApp.Models.Communication;
using EventApp.Models.Mappings;
using EventApp.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EventApp.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UserController(IUserService userService, IMapper mapper){
            _userService = userService;
            _mapper = mapper;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginRequest data)
        {
            var result = await _userService.LoginAsync(data);
            return Ok(result);
        }

        [HttpPost]
        [AllowAnonymous]
        /**
         * Test personok a különböző funkciók teszteléséhez
         *   insert into [EventAppDb].[dbo].[People] (Name, DateOfBirth, ApplicationUserId, BirthTown)
                values('Admin Admin',	'1900-01-01' ,1	,'Veszprem')
             insert into [EventAppDb].[dbo].[People] (Name, DateOfBirth, ApplicationUserId, BirthTown)
                values('User User',	'1995-01-01' ,2	,'Veszprem')
         */
        public async Task<IActionResult> Init(){
            await _userService.InitAsync();
            return Ok();
        }
    }
}
