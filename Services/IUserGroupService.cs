using System.Linq;
using System.Threading.Tasks;
using EventApp.Models;

namespace EventApp.Services
{
    public interface IUserGroupService
    {
        Task<UserGroup> CreateUserGroupAsync(UserGroup userGroup);

        Task UpdateUserGroupAsync(UserGroup userGroup);

        Task DeleteUserGroupAsync(int personId);

        IQueryable<UserGroup> GetUserGroup();

        Task<Person> GetUserGroupAsync(int userGroupId);
    }
}