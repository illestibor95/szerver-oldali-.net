using System.Linq;
using System.Threading.Tasks;
using EventApp.Models;
using EventApp.UnitOfWork;
using Microsoft.EntityFrameworkCore;

namespace EventApp.Services
{
    public class GroupService: IGroupService
    {
        private readonly IUnitOfWork _unitOfWork;

        // public EventService(EventAppDbContext context){
        //     _context = context;
        // }

        public GroupService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        
        public async Task<Group> CreateGroupAsync(Group group)
        {
            await _unitOfWork.GetRepository<Group>().Create(group);
            await _unitOfWork.SaveChangesAsync();
            return group;
        }

        public async Task UpdateGroupAsync(Group group)
        {
            await _unitOfWork.GetRepository<Group>().Update(group.Id, group);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteGroupAsync(int groupId)
        {
            await _unitOfWork.GetRepository<Group>().Delete(groupId);
            await _unitOfWork.SaveChangesAsync();
        }

        public IQueryable<Group> GetGroups()
        {
            return _unitOfWork.GetRepository<Group>().GetAll();
        }

        public async Task<Group> GetGroupAsync(int groupID)
        {
            return await  _unitOfWork.GetRepository<Group>().GetAll().Include(group => group.UserGroups).Include(group => group.Administrator.ApplicationUser).Where(group => group.Id == groupID).FirstAsync();
        }
    }
}