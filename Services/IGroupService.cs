using System.Linq;
using System.Threading.Tasks;
using EventApp.Models;

namespace EventApp.Services
{
    public interface IGroupService
    {
        Task<Group> CreateGroupAsync(Group group);

        Task UpdateGroupAsync(Group group);

        Task DeleteGroupAsync(int groupId);
        
        IQueryable<Group> GetGroups();

        Task<Group> GetGroupAsync(int groupID);

    }
}