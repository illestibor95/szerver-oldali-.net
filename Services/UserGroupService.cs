
using System.Linq;
using System.Threading.Tasks;
using EventApp.Models;
using EventApp.Repository;
using EventApp.UnitOfWork;

namespace EventApp.Services
{
    public class UserGroupService: IUserGroupService
    {
        private readonly IPersonRepository _personRepository;

        private readonly IUnitOfWork _unitOfWork;

        public UserGroupService(IPersonRepository personRepository, IUnitOfWork unitOfWork)
        {
            _personRepository = personRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<UserGroup> CreateUserGroupAsync(UserGroup userGroup)
        {
            await _unitOfWork.GetRepository<UserGroup>().Create(userGroup);
            await _unitOfWork.SaveChangesAsync();
            return userGroup;
        }

        public async Task UpdateUserGroupAsync(UserGroup userGroup)
        {
            throw new System.NotImplementedException();
        }

        public async Task DeleteUserGroupAsync(int userGroupId)
        {
            await _unitOfWork.GetRepository<UserGroup>().Delete(userGroupId);
        }

        public IQueryable<UserGroup> GetUserGroup()
        {
            return _unitOfWork.GetRepository<UserGroup>().GetAll();
        }

        public Task<Person> GetUserGroupAsync(int userGroupId)
        {
            throw new System.NotImplementedException();
        }
    }
}