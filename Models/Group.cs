using System.Collections.Generic;

namespace EventApp.Models
{
    public class Group : AbstractEntity
    {
        public string GroupName { get; set; }
        /*
         * 3. feladat Administrátor a grouphoz
         */
        public Person Administrator { get; set; }

        public int ?AdministratorId { get; set; }
        
        /**
         * 3. feladat n-n kapcsolat user és group között
         */
        public List<UserGroup> UserGroups { get; set; }

        public string Town { get; set; }
        public int GroupType { get; set; }
    }
}