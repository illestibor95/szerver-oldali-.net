using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace EventApp.Models {
    public class EventAppDbContext : IdentityDbContext<ApplicationUser, IdentityRole<int>, int> {
        
        public DbSet<Event> Events { get; set; }

        public DbSet<Place> Places { get; set; }

        public DbSet<Person> People { get; set; }

        public DbSet<Invitation> Invitations { get; set; }
        
        public DbSet<GroupInvitation> GroupInvitations { get; set; }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }

        public DbSet<ApiLogEntry> ApiLogEntries { get; set; }
        
        public DbSet<Group> Group { get; set; }
        
        public DbSet<UserGroup> UserGroup { get; set; }

        public EventAppDbContext(DbContextOptions<EventAppDbContext> options) : base(options){}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {           
            modelBuilder.Entity<Invitation>().HasKey(i => new { i.EventId, i.PersonId});

            modelBuilder.Entity<GroupInvitation>().HasKey(gi => new { gi.GroupId, gi.PersonId});

            modelBuilder.Entity<Person>().HasIndex(p => new {p.Name, p.DateOfBirth});
            modelBuilder.Entity<Event>().HasOne(e => e.Place).WithMany(p => p.Events).HasForeignKey(e => e.PlaceIdentity);

            //modelBuilder.Entity<Person>().HasMany(p => p.Friends).WithOne(p => p).HasForeignKey(p => p.Id);
            modelBuilder.Entity<Friend>().HasKey(f => new {f.Id});
            modelBuilder.Entity<Friend>().HasOne(f => f.Person).WithMany(p => p.Friends).HasForeignKey(f => f.PersonId).OnDelete(DeleteBehavior.Restrict);
            
            modelBuilder.Entity<EventStaff>().HasKey(es => new { es.EventId, es.OrganizerId });
            modelBuilder.Entity<EventStaff>().HasOne(es => es.Event).WithMany(e => e.Staff).HasForeignKey(es => es.EventId);
            modelBuilder.Entity<EventStaff>().HasOne(es => es.Organizer).WithMany(o => o.Events).HasForeignKey(es => es.OrganizerId);
               
            
            modelBuilder.Entity<UserGroup>()
                .HasKey(ug => new {ug.GroupId, ug.UserId});

            modelBuilder.Entity<UserGroup>()
                .HasOne(gu => gu.Person)
                .WithMany(p => p.UserGroups)
                .HasForeignKey(gu => gu.UserId).OnDelete(DeleteBehavior.SetNull);
            
            
            modelBuilder.Entity<UserGroup>()
                .HasOne(gu => gu.Group)
                .WithMany(p => p.UserGroups)
                .HasForeignKey(gu => gu.GroupId).OnDelete(DeleteBehavior.SetNull);
            modelBuilder.RemoveOneToManyCascadeDeleteConvention();
            base.OnModelCreating(modelBuilder);
        }
    }

    public static class ModelBuilderExtensions
    {        
        public static void RemoveOneToManyCascadeDeleteConvention(this ModelBuilder modelBuilder)
        {
            var cascadeFKs = modelBuilder.Model.GetEntityTypes()
                .SelectMany(t => t.GetForeignKeys())
                .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var fk in cascadeFKs)
                fk.DeleteBehavior = DeleteBehavior.Restrict;
        }
    }
}