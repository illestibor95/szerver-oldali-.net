namespace EventApp.Models
{
    public class UserGroup: AbstractEntity
    {
              
        /**
         * 3. feladat n-n kapcsolat user és group között
         */
        public int ?UserId { get; set; }
        public Person Person { get; set; }

        public int ?GroupId { get; set; }
        public Group Group { get; set; }
    }
}