using System;
using System.Collections.Generic;

namespace EventApp.Models {
    public class Person : AbstractEntity {
        public string Name { get; set; }

        public DateTime DateOfBirth { get; set; }

       //public List<Person> Friends { get; set; }
       public List<Friend> Friends { get; set; }
       
        //első feladat 1-1 kapcsolat az ApplicationUserrel
       public ApplicationUser ApplicationUser { get; set; }
       public int ApplicationUserId { get; set; }
       
       //2. feladat szülőváros attributum hozzáadása (dotnet ef update database)
       public string BirthTown { get; set; }
      
       /**
        * 3. feladat n-n kapcsolat user és group között
        */
       public List<UserGroup> UserGroups { get; set; }

    }
}