﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EventApp.Migrations
{
    public partial class MigrationName1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "UserGroups",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "AdministratorId",
                table: "Groups",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Groups",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Groups_AdministratorId",
                table: "Groups",
                column: "AdministratorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Groups_People_AdministratorId",
                table: "Groups",
                column: "AdministratorId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Groups_People_AdministratorId",
                table: "Groups");

            migrationBuilder.DropIndex(
                name: "IX_Groups_AdministratorId",
                table: "Groups");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "UserGroups");

            migrationBuilder.DropColumn(
                name: "AdministratorId",
                table: "Groups");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Groups");
        }
    }
}
