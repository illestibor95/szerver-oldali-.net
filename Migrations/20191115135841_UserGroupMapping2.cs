﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EventApp.Migrations
{
    public partial class UserGroupMapping2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserGroups_AspNetUsers_ApplicationUserId",
                table: "UserGroups");

            migrationBuilder.DropForeignKey(
                name: "FK_UserGroups_People_PersonId",
                table: "UserGroups");

            migrationBuilder.DropIndex(
                name: "IX_UserGroups_ApplicationUserId",
                table: "UserGroups");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "UserGroups");

            migrationBuilder.RenameColumn(
                name: "PersonId",
                table: "UserGroups",
                newName: "UserId");

            migrationBuilder.RenameIndex(
                name: "IX_UserGroups_PersonId",
                table: "UserGroups",
                newName: "IX_UserGroups_UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserGroups_AspNetUsers_UserId",
                table: "UserGroups",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserGroups_AspNetUsers_UserId",
                table: "UserGroups");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "UserGroups",
                newName: "PersonId");

            migrationBuilder.RenameIndex(
                name: "IX_UserGroups_UserId",
                table: "UserGroups",
                newName: "IX_UserGroups_PersonId");

            migrationBuilder.AddColumn<int>(
                name: "ApplicationUserId",
                table: "UserGroups",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserGroups_ApplicationUserId",
                table: "UserGroups",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserGroups_AspNetUsers_ApplicationUserId",
                table: "UserGroups",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserGroups_People_PersonId",
                table: "UserGroups",
                column: "PersonId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
